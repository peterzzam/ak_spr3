#include <msp430x14x.h>
#include "lcd.h"
#include "portyLcd.h"

#define DINO 0
#define KAKTUS 1
#define PUSTE 2
#define GORA 1
#define DOL 2

char highNick[3] =
{
  'p',
  'z',
  'a'
};
int highScore = 0;

void setup()
{
  P2DIR |= BIT1; // STATUS LED
  WDTCTL = WDTPW + WDTHOLD; // zatrzymanie WDT

  InitPortsLcd(); // inicjalizacja port�w
  InitLCD(); // inicjalizacja LCD
  clearDisplay(); // czyszczenie LCD
  P4DIR &= ~(BIT4 | BIT5 | BIT6 | BIT7);
}

//nasze
void ustawKursor(int linia, int n)
{
  if (linia == 2) {
    SEND_CMD(DD_RAM_ADDR2);
    SEND_CMD(DD_RAM_ADDR2 + n);
  } else {
    SEND_CMD(DD_RAM_ADDR);
    SEND_CMD(DD_RAM_ADDR + n);
  }
}

void wyswietlChar(char znak)
{
  SEND_CHAR(znak);
}

void czekaj(int ms)
{
  Delayx100us(ms);
}

void wyswietlString(char * string, int n)
{
  for (int i = 0; i < n; i++)
  {
    SEND_CHAR(string[i]);
  }
}

void wyswietlInt(int licznik)
{
  int setki = licznik / 100;
  int dziesiatki = (licznik - setki * 100) / 10;
  int jednosci = licznik - setki * 100 - dziesiatki * 10;
  SEND_CHAR(setki + 48);
  SEND_CHAR(dziesiatki + 48);
  SEND_CHAR(jednosci + 48);
}

void main(void)
{
  setup();
  int highScore = 0;
  int licznik = 0;
  char nick[4] =
  {
    'a',
    'a',
    'a',
    'a'
  };

  while (1)
  {
    while (1)
	{
      //dioda
      P2DIR |= BIT1; //konfiguracja bitu 1 jako wyjscie
      P1DIR |= (BIT5 | BIT6);
      P4DIR &= ~(BIT4 | BIT5 | BIT6);
      //menu
      ustawKursor(GORA, 0);
      wyswietlString("1 Play 2 HiSc", 13);
      ustawKursor(DOL, 0);
      wyswietlString("3 Desc 4 Auth", 13);
      czekaj(1000);
      ustawKursor(GORA, 0);
      wyswietlString(" ", 16);
      ustawKursor(DOL, 0);
      wyswietlString(" ", 16);
      if ((P4IN & BIT4) == 0)
	  {
        break; // zapala d status
      }
	  else if ((P4IN & BIT5) == 0)
	  {
        ustawKursor(GORA, 0);
        wyswietlString(highNick, 2);
        ustawKursor(DOL, 0);
        wyswietlInt(highScore);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
      }
	  else if ((P4IN & BIT6) == 0)
	  {
        ustawKursor(GORA, 0);
        wyswietlString("Skacz dino", 10);
        ustawKursor(DOL, 0);
        wyswietlString("unikaj kaktusow", 15);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
      }
	  else if ((P4IN & BIT7) == 0)
	  {
        ustawKursor(GORA, 0);
        wyswietlString("PZ, DR, PS", 10);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
        czekaj(5000);
      }
	  else
	  {
        continue;
      }
      ustawKursor(GORA, 0);
      wyswietlString(" ", 16);
      ustawKursor(DOL, 0);
      wyswietlString(" ", 16);
    }

    //wpisywanie nicku
    int i = 0;
    int j = 0;
    ustawKursor(DOL, j);
    while ((P4IN & BIT7) != 0)
	{
      if ((P4IN & BIT4) == 0 && i > 0)
	  {
        i--;
        wyswietlChar(97 + i);
        ustawKursor(DOL, j);
      } else if ((P4IN & BIT5) == 0 && i < 25)
	  {
        i++;
        wyswietlChar(97 + i);
        ustawKursor(DOL, j);
      } else if ((P4IN & BIT6) == 0 && j < 2)
	  {
        j++;
        ustawKursor(DOL, j);
        czekaj(1000);
        //*(nick+j-1) = 97 + i;
        nick[j - 1] = 97 + i;
        nick[j - 1];
        i = 0;
      }
      czekaj(100);
    }
    ustawKursor(DOL, 0);
    wyswietlString(" ", 16);

    SEND_CMD(CG_RAM_ADDR);
    char dino[] =
	{
      0x00,
      0x07,
      0x06,
      0x07,
      0x0E,
      0x1F,
      0x1E,
      0x0A
    };
    char kaktus[] =
	{
      0x04,
      0x0E,
      0x04,
      0x14,
      0x1D,
      0x07,
      0x04,
      0x04
    };
    char puste[] =
	{
      0x00,
      0x00,
      0x00,
      0x00,
      0x00,
      0x00,
      0x00,
      0x00
    };
    for (int i = 0; i < 8; i++)
	{
      SEND_CHAR(dino[i]);
    }
    for (int i = 0; i < 8; i++)
	{
      SEND_CHAR(kaktus[i]);
    }
    for (int i = 0; i < 8; i++)
	{
      SEND_CHAR(puste[i]);
    }

    SEND_CMD(DD_RAM_ADDR);

    i = 0, j = -1;
    int k = -1;

    while (1)
	{
      if (i == 10) j = 0;
      if (i == 5) k = 0;
      //wyswietlanie kaktusow
      ustawKursor(DOL, 15 - i % 16);
      wyswietlChar(KAKTUS);
      if (j != -1)
	  {
        ustawKursor(DOL, 15 - j % 16);
        wyswietlChar(KAKTUS);
      }
      if (k != -1)
	  {
        ustawKursor(DOL, 15 - k % 16);
        wyswietlChar(KAKTUS);
      }
      //czekanie
      czekaj(250);
      //czyszczenie kaktusow
      ustawKursor(DOL, 15 - i % 16);
      wyswietlChar(PUSTE);
      if (j != -1)
	  {
        ustawKursor(DOL, 15 - j % 16);
        wyswietlChar(PUSTE);
        j++;
      }
      if (k != -1)
	  {
        ustawKursor(DOL, 15 - k % 16);
        wyswietlChar(PUSTE);
        k++;
      }
      i++;
      //ustawianie dino
      if ((P4IN & BIT4) == 0)
	  {
        //zapalanie diody
        P2OUT = P2OUT & ~BIT1;
        ustawKursor(DOL, 0);
        wyswietlChar(PUSTE);
        ustawKursor(GORA, 0);
        wyswietlChar(DINO);
      }
	  else
	  {
        //gaszenie diody
        P2OUT = P2OUT | BIT1;
        ustawKursor(GORA, 0);
        wyswietlChar(PUSTE);
        ustawKursor(DOL, 0);
        wyswietlChar(DINO);
      }
      //sprawdzanie kolizji
      if (((15 - i % 16 == 0 || 15 - j % 16 == 0 || 15 - k % 16 == 0) && (P4IN & BIT4) != 0) || licznik == 10)
	  {
        break;
      }
      if ((15 - i % 16 == 0 || 15 - j % 16 == 0 || 15 - k % 16 == 0) && (P4IN & BIT4) == 0)
	  {
        licznik++;
      }
    }
    ustawKursor(GORA, 0);
    wyswietlString("Game over", 9);
    ustawKursor(DOL, 0);
    wyswietlString("Your score: ", 12);
    ustawKursor(DOL, 12);
    wyswietlInt(licznik);
    if (licznik > highScore)
	{

      for (int i = 0; i < 2; i++)
	  {
        highNick[i] = nick[i];
      }
      highScore = licznik;
    }
    czekaj(5000);
    czekaj(5000);
    czekaj(5000);
    czekaj(5000);
    czekaj(5000);
    czekaj(5000);
    czekaj(5000);
    wyswietlString(" ", 16);
  }
}
